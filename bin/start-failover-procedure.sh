#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

export SKIP_HOST_CHECK=true

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
# shellcheck disable=SC1091,SC1090
source "${SCRIPT_DIR}/workflow-script-commons.sh"

# --------------------------------------------------------------

function createIssue() {
  local title
  local template
  local body
  title=$1
  template=$2
  body=$(cat "${SCRIPT_DIR}/../.gitlab/issue_templates/${template}.md")

  echo "Creating ${title} with template ${template}"
  while read -r line; do
    # shellcheck disable=SC2001
    body=$(echo "$body"| sed "s#__${line}__#${!line}#")
  done < <(grep -Eho 'export \w+' "${SCRIPT_DIR}/source_vars_template.sh"|cut -d" " -f2)

  PROJECT_ID=${GITLAB_MIGRATION_PROJECT_PATH/\//%2f}
  curl --silent --fail --request POST -H "Private-Token: ${GITLAB_TOKEN}" "${GITLAB_INSTANCE}/api/v4/projects/${PROJECT_ID}/issues" --data-urlencode "title=${title}" --data-urlencode "description=${body}" > /dev/null
}

if [[ ${FAILOVER_ENVIRONMENT} == "stg" ]]; then
  name="${FAILOVER_DATE} STAGING failover attempt:"
else
  name="${FAILOVER_DATE} PRODUCTION failover attempt:"
fi

createIssue "${name} preflight checks" "preflight_checks"
createIssue "${name} main procedure" "failover"
createIssue "${name} test plan" "test_plan"
createIssue "${name} failback" "failback"
